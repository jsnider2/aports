# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=rqlite
pkgver=7.14.1
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all !riscv64" # ftbfs
license="MIT"
makedepends="go sqlite-dev"
checkdepends="python3 py3-requests"
options="!check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench
	go build ./cmd/rqlite
	go build ./cmd/rqlited
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin

	install -D -m644 "$builddir"/DOC/*.md -t \
		"$pkgdir"/usr/share/doc/$pkgname
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}

client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
dd3493ef73ddbe6cd46963a3d35a32fa97330652742095f75b57c49ec2a0d8a35ec609e5b8df673bda842178a8858fb9885f234a840340112819924cac9e0289  rqlite-7.14.1.tar.gz
"

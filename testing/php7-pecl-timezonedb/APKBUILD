# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Fabio Ribeiro <fabiorphp@gmail.com>
pkgname=php7-pecl-timezonedb
_extname=timezonedb
pkgver=2023.1
pkgrel=0
pkgdesc="Timezone Database to be used with PHP's date and time functions."
url="https://pecl.php.net/package/timezonedb"
arch="all"
license="PHP-3.01"
depends="php7-common"
makedepends="php7-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"
provides="php7-timezonedb=$pkgver-r$pkgrel" # for backward compatibility
replaces="php7-timezonedb" # for backward compatibility

build() {
	phpize7
	./configure --prefix=/usr --with-php-config=php-config7
	make
}

check() {
	# Test suite is not a part of pecl release.
	php7 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php7/conf.d
	install -d $_confdir
	echo "extension=$_extname.so" > $_confdir/40_$_extname.ini
}

sha512sums="
c10039c529e1925bf8c9e5f97e5e3a8cbd11a7ab9c415da01dd4371e531ce851b6cb23274de57b679dbafb1318bef6691118ff39888cad3b2acab2e665316db5  php-pecl-timezonedb-2023.1.tgz
"

# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gegl
pkgver=0.4.42
pkgrel=2
pkgdesc="Graph based image processing framework"
url="https://www.gegl.org/"
arch="all"
license="GPL-3.0-or-later AND LGPL-3.0-or-later"
makedepends="
	babl-dev
	ffmpeg-dev
	gdk-pixbuf-dev
	gobject-introspection-dev
	json-glib-dev
	libjpeg-turbo-dev
	libpng-dev
	libraw-dev
	librsvg-dev
	libwebp-dev
	meson
	pango-dev
	vala
	"
checkdepends="diffutils"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gimp.org/pub/gegl/${pkgver%.*}/gegl-$pkgver.tar.xz
	$pkgname-invalid-free.patch::https://gitlab.gnome.org/GNOME/gegl/-/commit/f2f2cea219f2265e152e3308004dc98d2fd48f33.patch
	arm-neon-v1.patch
	"

# secfixes:
#   0.4.34-r0:
#     - CVE-2021-45463

case "$CARCH" in
s390x)
	# gegl_tile segfaults
	options="$options !check"
	;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
29ccd93faa6127b6a9b1180eeba47f1b6f9bc06da8a5c9d68315d7474b77ff19d1084a6f4e7b218666b355329b17bc562f5906a334aa2baf93840d7caa0adf57  gegl-0.4.42.tar.xz
8c01e53306060d48d4dfe94d0cfdebb9b49c3104664286f7394c838199b20e836103f133d7d411fcde839dec0494ebc2734cf560ac4fb19bb0c67e6f78eddfff  gegl-invalid-free.patch
add122d409354b3aaa205adaa6fac5003fbc83f23ffcf5d230edf2b390b36c168fbfd89c47a5bdf41c06254403eff4347c2c667e18d84cdf00857e743c29b03b  arm-neon-v1.patch
"

# Maintainer: kpcyrd <git@rxv.cc>
pkgname=osv-scanner
pkgver=1.2.0
pkgrel=0
pkgdesc="Vulnerability scanner written in Go which uses the data provided by https://osv.dev"
url="https://github.com/google/osv-scanner"
arch="all"
license="Apache-2.0"
makedepends="go"
options="net"
source="https://github.com/google/osv-scanner/archive/v$pkgver/osv-scanner-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	# workaround to not have a gitignore for tests
	git init .
}

build() {
	mkdir build
	go build -o build/ ./cmd/...
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./build/osv-scanner -t "$pkgdir"/usr/bin/
}

sha512sums="
c2720dd519bf56b2eccf3b5930ba0d7faaebb613d9adf20a0072c545e058814c24f350c2fec72a74ee9622246d39e8769b88e3ea96ae751afeb6cffb1549711b  osv-scanner-1.2.0.tar.gz
"

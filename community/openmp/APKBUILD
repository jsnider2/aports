# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=openmp
pkgver=16.0.0
pkgrel=1
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
arch="all !s390x" # LIBOMP_ARCH = UnknownArchitecture
license="Apache-2.0"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	$pkgname-bitcode=$pkgver-r$pkgrel
	"
makedepends="
	clang
	cmake
	elfutils-dev
	libffi-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
checkdepends="llvm$_llvmver-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/openmp-${pkgver//_/}.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/cmake-${pkgver//_/}.src.tar.xz
	"
builddir="$srcdir/$pkgname-${pkgver//_/}.src"
options="!check" # todo

case "$CARCH" in
aarch64|ppc64le|x86_64)
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
		libomptarget-rtl-cuda
		libomptarget-rtl-cuda-nextgen:cuda_nextgen
		libomptarget-rtl-amdgpu
		libomptarget-rtl-amdgpu-nextgen:amdgpu_nextgen
		libomptarget-rtl-nextgen
		libomptarget-rtl
		"
	;;
riscv64)
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
	"
esac

prepare() {
	default_prepare
	mv "$srcdir"/cmake-${pkgver//_/}.src "$srcdir"/cmake
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CC=clang \
	CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCLANG_TOOL=/usr/bin/clang \
		-DLINK_TOOL=/usr/bin/llvm-link \
		-DOPT_TOOL=/usr/bin/opt \
		-DPACKAGER_TOOL=/usr/bin/clang-offload-packager \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

libomptarget() {
	amove usr/lib/libomptarget.so.*
}

rtl() {
	amove usr/lib/libomptarget.rtl.*.so.*
}

nextgen() {
	amove usr/lib/libomptarget.rtl.*.nextgen.so.*
}

amdgpu() {
	amove usr/lib/libomptarget.rtl.amdgpu.so.*
}

amdgpu_nextgen() {
	amove usr/lib/libomptarget.rtl.amdgpu.nextgen.so.*
}

cuda_nextgen() {
	amove usr/lib/libomptarget.rtl.cuda.nextgen.so.*
}

cuda() {
	amove usr/lib/libomptarget.rtl.cuda.so.*
}

bitcode() {
	amove usr/lib/libomptarget*.bc
}

sha512sums="
ea094cdf5ebc590ebe91c083bbef4164a3ad98861bfad2acfc7532e1c5a82411f606db9cef93dfc2bffbfc66f33b94b563e816647c127366496c15ab08bbc3ec  openmp-16.0.0.src.tar.xz
4f21461aa8165061dbea47dcda4f098957e16bd307484bcb66884cf5a0776197f69a74002d5601229c4630db53ac44049f3f2ce1e96a6bb16ba3df828d387932  cmake-16.0.0.src.tar.xz
"
